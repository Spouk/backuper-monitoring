package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

type BackuperServerMonitoring struct {
	log     *log.Logger
	c       Config
	stockCM *stockClientMessage
}

func Init() Config {
	flag.Usage = func() {
		fmt.Println(title)
		flag.PrintDefaults()
	}
	cfile := flag.String("config", "", "config file name with path")
	flag.Parse()
	if len(os.Args[1:]) != 1 {
		flag.PrintDefaults()
		os.Exit(1)
	}
	c := Config{}
	if err := c.readconfigfromfile(*cfile); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	return c
}
func NewBackuperServerMonitoring(cfg Config) (*BackuperServerMonitoring, error) {
	bsm := &BackuperServerMonitoring{
		log: log.New(os.Stdout, prefixlog, log.LstdFlags),
		c:   cfg,
	}
	//read config file
	fh, err := bsm.OpenLogFile(bsm.c.Logfile)
	if err != nil {
		bsm.log.Fatal(err)
	}
	bsm.log.SetOutput(io.MultiWriter(os.Stdout, fh))

	//make global stock event messages
	bsm.stockCM = newstockClientMessage(bsm.c.Timehourclearbuffer, bsm.log)

	return bsm, nil
}
func (b *BackuperServerMonitoring) Run() {
	serv, err := net.Listen("tcp", fmt.Sprintf("%s:%s", b.c.Serverip, b.c.Serverport))
	if err != nil {
		b.log.Fatal(err)
	}
	b.log.Printf("-- сервер мониторинга запущен на %v : %v\n", b.c.Serverip, b.c.Serverport)
	for {
		con, err := serv.Accept()
		if err != nil {
			b.log.Printf("-- ошибка удаленного подключения  %v\n", err)
			continue
		}
		go b.worker(con)
		if b.c.Debug {
			res, _ := b.stockCM.GetLastSlice(2)
			b.log.Printf("-> %v\n\r-> %v\n", res, b.stockCM.GetAll())
		}
	}
}
func (b *BackuperServerMonitoring) worker(conn net.Conn) {
	defer func() {
		if b.c.Debug {
			b.log.Printf("-- потерян коннект с %v\n", conn.RemoteAddr().String())
		}
	}()
	if b.c.Debug {
		b.log.Printf("-- установлен коннект с %v\n", conn.RemoteAddr().String())
	}

	//пробую получить от клиента сообшение
	dec := gob.NewDecoder(conn)
	res := &clientMessage{}
	err := dec.Decode(res)
	if err != nil {
		b.log.Printf("-- ошибка в декодировании сообщения от клиента %v : %v\n", conn.RemoteAddr().String(), err)
		_ = conn.Close()
		return
	}
	//формирую имя выходного файла
	fname := strings.Join([]string{res.NodeIP, strings.Join(res.NodeAliasDB, "_"), ".log"}, "_")
	//создаю/открываю файл
	h, err := b.OpenLogFile(fname)
	if err != nil {
		b.log.Printf("-- ошибка при открытии/создании файла %v : %v\n", conn.RemoteAddr(), err)
		return
	}
	//формирую выходное сообщение
	outmsg := fmt.Sprintf("%v    ->    %v:%v (%v) [%v]  =  %v\n", res.EventTime, res.NodeIP, res.NodePort, res.NodeAliasDB, res.NodeAlias, res.EventName)
	b.log.Printf("--> %v", fmt.Sprintf("%v %v:%v (%v) [%v]  =  %v\n", res.EventTime, res.NodeIP, res.NodePort, res.NodeAliasDB, res.NodeAlias, res.EventName))
	//записываю сообщение в лог
	_, err = h.WriteString(outmsg)
	if err != nil {
		b.log.Printf("-- ошибка при записи в файл сообщения %v : %v\n", fname, err)
	}
	if b.c.Debug {
		b.log.Printf("-- успешно записано сообшение в  файл  %v\n", fname)
	}
	//запись сообщения в общий сток
	b.stockCM.Add(res)
}
