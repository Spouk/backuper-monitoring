package main

import (
	"errors"
	"github.com/go-yaml/yaml"
	"log"
	"os"
	"sync"
	"time"
)

type Config struct {
	Serverip            string        `yaml:"serverip"`
	Serverport          string        `yaml:"serverport"`
	Checktimeclient     time.Duration `yaml:"checktimeclient"`
	Debug               bool          `yaml:"debug"`
	Logfile             string        `yaml:"logfile"`
	Pathlogclients      string        `yaml:"pathlogclients"`
	Timehourclearbuffer time.Duration
}
type clientMessage struct {
	NodeIP      string   `yaml:"nodeip"`
	NodePort    string   `yaml:"nodeport"`
	NodeAliasDB []string `yaml:"nodeAliasDB"` //все базы которые обслуживаются бэкапером на этом ip
	NodeAlias   string   `yaml:"NodeAlias"`   // конкретный алиас базы данных по которому проходит действие
	NodeUptime  string   `yaml:"NodeUptime"`
	EventName   string   `yaml:"EventName"` //тип события несет в себе несколько смыслов вид события + время поступления события = время начала события(
	EventTime   string   `yaml:"EventTime"`
}
type stockClientMessage struct {
	sync.Mutex
	stock []*clientMessage //
}

// создает  структуру + запускает горутину которая очишает структуру с периодичностью по таймеру, можно было конечно fifo замутить с лимитированным буфером и ротацией, но хочу так
func newstockClientMessage(timehours time.Duration, logger *log.Logger) *stockClientMessage {
	s := &stockClientMessage{
		Mutex: sync.Mutex{},
		stock: []*clientMessage{},
	}
	go func(tc time.Duration, l *log.Logger, su *stockClientMessage) {
		l.Printf("--горутина по очистке буфера запущена с периодом %v часов, следующая очистка %v\n", tc, time.Now().Add(tc*time.Hour).String())
		ticker := time.NewTicker(time.Hour * tc)
		for {
			select {
			case _ = <-ticker.C:
				l.Printf("-- время очистки буфера....\n")
				su.ClearStock()
				l.Printf("-- буфер успешно очищен....\n")
			}
		}
	}(timehours, logger, s)
	return s
}

// добавить элемент в сток
func (s *stockClientMessage) Add(c *clientMessage) {
	s.Lock()
	defer s.Unlock()
	s.stock = append(s.stock, c)
}

// полная очистка списка - время ротации обнуления задается через конфиг
func (s *stockClientMessage) ClearStock() {
	s.Lock()
	defer s.Unlock()
	if len(s.stock) == 0 {
		return
	}
	s.stock = []*clientMessage{}
}
func (s *stockClientMessage) GetLastSlice(count int) ([]*clientMessage, error) {
	s.Lock()
	defer s.Unlock()
	if len(s.stock) == 0 {
		return nil, errors.New("список пустой!")
	}
	if count >= len(s.stock) {
		return s.stock, nil
	}
	return s.stock[len(s.stock)-count:], nil
}
func (s *stockClientMessage) GetAll() []*clientMessage {
	s.Lock()
	defer s.Unlock()
	return s.stock
}

//eventname = createbackup, endcreatebackup, createrestor, endcreaterestor, arc, endarc, arccheck,endarccheck, copyready, endcopyready, copyremote,endcopyremote, swap, endswap etc...

func (c *Config) readconfigfromfile(filenames string) error {
	fh, err := os.Open(filenames)
	if err != nil {
		return err
	}
	defer fh.Close()
	dec := yaml.NewDecoder(fh)
	err = dec.Decode(c)
	if err != nil {
		return err
	}
	return nil
}

func NewConfig(filename string) (*Config, error) {
	i := &Config{}
	err := i.readconfigfromfile(filename)
	if err != nil {
		return nil, err
	}
	return i, err
}
