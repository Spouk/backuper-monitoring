package main

func main() {
	b, err := NewBackuperServerMonitoring(Init())
	if err != nil {
		panic(err)
	}
	b.Run()
}
