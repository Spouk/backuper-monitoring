package main

import (
	"io/fs"
	"os"
)

func (p *BackuperServerMonitoring) OpenLogFile(filename string) (*os.File, error) {
	if stat, err := os.Stat(filename); err != nil {
		if err == os.ErrNotExist {
			fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		} else {
			if _, ok := err.(*fs.PathError); ok {
				fh, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, os.ModePerm)
				if err != nil {
					return nil, err
				}
				return fh, nil
			}
			return nil, err
		}
	} else {
		if !stat.IsDir() {
			fh, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, os.ModePerm)
			if err != nil {
				return nil, err
			}
			return fh, nil
		}
	}
	return nil, nil
}
